This layer depends on:

    URI: git://git.yoctoproject.org/poky.git
    branch: morty
    revision: HEAD
    commit: 8a96509

    URI: git://git.openembedded.org/meta-openembedded
    branch: morty
    revision: HEAD
    commit: fe5c833

    URI: https://github.com/meta-qt5/meta-qt5.git
    branch: morty
    revision: HEAD
    commit: 3601fd2

    URI: git://git.yoctoproject.org/meta-raspberrypi 
    branch: morty
    revision: HEAD
    commit: 380bf2f

    meta-rpi layer maintainer: Scott Ellis <scott@jumpnowtek.com>
